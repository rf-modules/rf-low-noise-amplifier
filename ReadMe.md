This repository contains files of low noise amplifiers PCBs with or without filtering. They use two type of connectors, SMA or N-type.
The amplifiers operate at UHF and S-Band. Files are described below:

# LNA S-Band

Low noise amplifier operating in S-Band with a 2.1-2.9 GHz bandpass filter.
Configuration: Filtering before or after amplification.
SMA connectors

# LNA UHF N-type BFTC-500+

Low noise amplifier with a 400-600 Mhz bandpass filter.
Configuration: Filtering before amplification
N-type connectors

# LNA UHF N-type

Low noise amplifier with a 433 MHz filter
Configuration: Filtering before amplification
N-type connectors

# LNA UHF

Low noise amplifier with a 433 MHz filter.
Configuration: Filtering before amplification
N-type connectors

# LNA UHF SMA

Similar to LNS UHF but with SMA connectors.
Configuration: Filtering before or after amplification.

# LNA Windband

Flat gain low noise amplifier at 0.6-4.2 GHz without filtering.
SMA connectors